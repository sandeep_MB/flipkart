import { combineReducers } from "redux";
import productReducer from "./Reducer";

export default combineReducers({
  productReducer,
});
