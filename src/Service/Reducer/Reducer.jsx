import { GET_PRODUCT, ADD_TO_CART } from "../Constants/Constants";

const initialState = {
  products: [],
  cardData: [],
  modal: false,
  error: true,
  loader: true,
};
export default function productReducer(state = initialState, action) {
  // console.log("action ", action.data);

  switch (action.type) {
    case GET_PRODUCT:
      return {
        ...state,
        products: action.data.products,
        error: action.data.error,
        loader: action.data.loader,
      };

    case ADD_TO_CART:
      console.warn("reducer", action.data);
      return {
        ...state,
        cardData: action.data,
      };
    default:
      return state;
  }
}
