import { GET_PRODUCT, ADD_TO_CART } from "../Constants/Constants";

export const getProducts = (data) => {
  return {
    type: GET_PRODUCT,
    data: data,
  };
};

export const addToCart = (data) => {
  // console.warn("action",data)
  return {
    type: ADD_TO_CART,
    data: data,
  };
};
