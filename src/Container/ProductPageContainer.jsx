import ProductPage from "../components/ProductPage/ProductPage";
import { connect } from "react-redux";
import { getProducts, addToCart } from "../Service/Actions/Action";

// console.log("getProducts", getProducts());

const mapStateToProps = (state) => ({
  data: state,
});
const mapDispatchToProps = (dispatch) => ({
  getProductData: (data) => dispatch(getProducts(data)),
  addDataToCart: (data) => dispatch(addToCart(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);
