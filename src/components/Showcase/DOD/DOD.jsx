import React, { Component } from "react";
import "./DOD.css";

export default class DOD extends Component {
  render() {
    return (
      <>
        <div className="d-flex justify-content-center align-items-center">
          <div className="dod m-4">
            <div className="px-4  py-2 d-flex justify-content-between align-items-center dodHeader">
              <div className="d-flex align-items-center">
                <h3>Deal of The Day</h3>
                <div className="dodTimer px-4 ">12:45:35</div>
              </div>

              <div>
                <div className="btn btn-primary">View More</div>
              </div>
            </div>
            <hr />
            <div className="dodSlider d-flex">
              <div className="dodProduct">
                <img
                  className="dodImage"
                  src="https://rukminim1.flixcart.com/image/150/150/kgv5x8w0/jacket/n/p/f/m-dtaw20jk023a-ducati-original-imafxyazyemfzzv4.jpeg?q=70"
                  alt="red t-shirt"
                />
                <h4 className="dodSlogans">Brands</h4>
                <p className="dodDiscounts">Min 50% OFF </p>
                <p className="dodCategory">Explore Now</p>
              </div>
              <div className="dodProduct">
                <img
                  src="https://rukminim1.flixcart.com/image/150/150/kskotjk0/blanket/y/w/d/single-bed-polar-fleece-ac-room-light-weight-blanket-red-color-original-imag63uhwpzhadn5.jpeg?q=70"
                  alt="red t-shirt"
                  className="dodImage"
                />
                <h3 className="dodSlogans">Blalnkets</h3>
                <p className="dodDiscounts">Min 70% OFF </p>
                <p className="dodCategory">Explore Now</p>
              </div>
              <div className="dodProduct">
                <img
                  src="https://rukminim1.flixcart.com/image/150/150/kq5iykw0/immersion-rod/0/9/i/2000-high-quality-redshell-original-imag48mhg8rynuhy.jpeg?q=70"
                  alt="red t-shirt"
                  className="dodImage"
                />
                <h3 className="dodSlogans  ">Rods</h3>
                <p className="dodDiscounts">Min 90% OFF </p>
                <p className="dodCategory">Explore Now</p>
              </div>
              <div className="dodProduct">
                <img
                  src="https://rukminim1.flixcart.com/image/150/150/kwtkxow0/poncho/e/l/q/free-pv-3604-navy-pivl-original-imag9ffvqdauhbxe.jpeg?q=70"
                  alt="red t-shirt"
                  className="dodImage"
                />
                <h3 className="dodSlogans">Ponchos</h3>
                <p className="dodDiscounts">Min 100% OFF </p>
                <p className="dodCategory">Explore Now</p>
              </div>
              <div className="dodProduct">
                <img
                  src="https://rukminim1.flixcart.com/image/150/150/kbi9h8w0/dress/n/3/f/l-ttj6005093-tokyo-talkies-original-imafsuc4d6qh9pp3.jpeg?q=70"
                  alt="red t-shirt"
                  className="dodImage"
                />
                <h3 className="dodSlogans  ">Dresses</h3>
                <p className="dodDiscounts">Min 10% OFF </p>
                <p className="dodCategory">Explore Now</p>
              </div>
              <div className="dodProduct">
                <img
                  src="https://rukminim1.flixcart.com/image/150/150/k26h0280/jacket/j/b/a/m-hljk000297-highlander-original-imafhhbqevhdastj.jpeg?q=70"
                  alt="red t-shirt"
                  className="dodImage"
                />
                <h3 className="dodSlogans">Jackets</h3>
                <p className="dodDiscounts">Min 50% OFF </p>
                <p className="dodCategory">Explore Now</p>
              </div>
              <div className="dodProduct">
                <img
                  src="https://rukminim1.flixcart.com/image/150/150/jd0jtzk0/cases-covers/back-cover/z/b/f/befunky-isimple-matte-finish-flexible-soft-tpu-skin-support-original-imafyfmje9mvkfea.jpeg?q=70"
                  alt="red t-shirt"
                  className="dodImage"
                />
                <h3 className="dodSlogans">Phones</h3>
                <p className="dodDiscounts">Min 90% OFF </p>
                <p className="dodCategory">Explore Now</p>
              </div>
            </div>
          </div>
          <div className="comingSoon d-flex justify-content-center align-items-center">
            <img
              className="img-fluid"
              src="https://rukminim1.flixcart.com/flap/464/708/image/218a49a602f21467.jpg?q=70"
              alt=""
            />
          </div>
        </div>
      </>
    );
  }
}
