import React, { Component } from "react";
// import img1 from "./img/carousel1.webp";
// import img2 from "./img/carousel2.webp";
// import img3 from "./img/carousel3.webp";
// import img4 from "./img/carousel4.png";
import img1 from "./img/img1.webp";
import img2 from "./img/img2.webp";
import img3 from "./img/img3.webp";
import img4 from "./img/img4.webp";
import img5 from "./img/img5.webp";

import "./Carousel.css";

export default class Carousel extends Component {
  render() {
    return (
      <div>
        <div
          id="carouselExampleControls"
          className="carousel slide"
          data-bs-ride="carousel"
        >
          <div className="carousel-inner">
            <div className="carousel-item active ">
              <img
                src={img1}
                className=" carousel-image d-block w-100"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src={img2}
                className="carousel-image d-block w-100"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src={img3}
                className="carousel-image d-block w-100"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src={img4}
                className="carousel-image d-block w-100"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src={img5}
                className="carousel-image d-block w-100"
                alt="..."
              />
            </div>
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    );
  }
}
