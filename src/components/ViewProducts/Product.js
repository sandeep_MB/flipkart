import "./Product.css";
import React from "react";
import { FaShoppingCart, FaTicketAlt } from "react-icons/fa";
import { FcFlashOn } from "react-icons/fc";
import { FaTag } from "react-icons/fa";
import Header from "../../Container/HeaderContainer";
import Footer from "../Footer/Footer";

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oneProduct: {},
    };
  }

  getProduct(id) {
    const products = this.props.data.productReducer.products;
    this.setState({ oneProduct: products[id - 1] });
  }

  componentDidMount() {
    const path = window.location.pathname;
    const index = path.split("/");
    const id = index[index.length - 1];
    this.getProduct(id);
  }

  render() {
    const product = this.state.oneProduct;
    return (
      <>
        <Header />
        <div className="product">
          <div className="productContainer">
            <div className="productImageButtons">
              <img className="productImage" src={product.image} alt="" />
              <div className="buttons">
                <button className="btnCart">
                  <FaShoppingCart /> ADD TO CART
                </button>
                <button className="btnBuy">
                  <FcFlashOn />
                  BUY NOW
                </button>
              </div>
            </div>
            <div className="productDetails">
              <p className="heading">{product.title}</p>
              <div className="price">
                <p
                  style={{
                    color: "#26a541",
                    textAlign: "left",
                    fontSize: "14px",
                    fontWeight: 500,
                    marginBottom: "10px",
                  }}
                >
                  Special Price
                </p>
                $ {product.price}
              </div>
              <div className="coupons">
                <p
                  style={{
                    color: "#212121",
                    textAlign: "left",
                    fontSize: "18px",
                    fontWeight: 500,
                    marginTop: "10px",
                  }}
                >
                  Coupons for you
                </p>
                <span>
                  <FaTicketAlt className="ticket" />
                  Special PriceGet extra 20% off upto ₹50 on 1 item(s) (price
                  inclusive of discount)T&C
                </span>
              </div>
              <br />
              <div className="offers d-flex flex-column align-items-baseline">
                <p
                  style={{
                    color: "#212121",
                    textAlign: "left",
                    fontSize: "18px",
                    fontWeight: 500,
                    marginTop: "10px",
                  }}
                >
                  Available offers
                </p>
                <li>
                  <FaTag className="tags" />
                  <span>
                    Special PriceGet extra 28% off (price inclusive of
                    discount)T&C
                  </span>
                </li>
                <li>
                  <FaTag className="tags" />
                  <span>
                    Bank Offer10% off on IndusInd Bank Cards, up to ₹250 . On
                    orders of ₹1000 and aboveT&C
                  </span>
                </li>
                <li>
                  <FaTag className="tags" />
                  <span>
                    Bank Offer10% off on Federal Bank Cards, up to ₹250 . On
                    orders of ₹1000 and aboveT&C
                  </span>
                </li>
                <li>
                  <FaTag className="tags" />
                  <span>
                    Bank Offer5% Unlimited Cashback on Flipkart Axis Bank Credit
                    CardT&C
                  </span>
                </li>
                <li>
                  <FaTag className="tags" />
                  <span>
                    {" "}
                    OfferSign up for Flipkart Pay Later and get Flipkart Gift
                    Card worth ₹100*Know More
                  </span>
                </li>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

export default Product;
