import React, { Component } from "react";
import HeaderContent from "../Header/HeaderContent/HeaderContent";
import Carousel from "../Carousel/Carousel";
import DOD from "../Showcase/DOD/DOD";
import ProductPage from "../../Container/ProductPageContainer";
import FooterContent from "../Footer/FooterContent/FooterContent";
import Header from "../../Container/HeaderContainer";
import Footer from "../Footer/Footer";

export default class Home extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    console.log(this.props);
    return (
      <div>
        <Header />
        <HeaderContent />
        <Carousel />
        <DOD />
        <ProductPage />
        <FooterContent />
        <Footer />
      </div>
    );
  }
}
