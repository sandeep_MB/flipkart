import React, { Component } from "react";
import brandImage from "./img/flipkart-plus_8d85f4.png";
import "./Header.css";
import Login from "../Login/Login";
import { Link } from "react-router-dom";

export default class Header extends Component {
  render() {
    const cardData = this.props.data.productReducer.cardData;
    console.log("length", cardData.length);
    return (
      <div>
        <nav className="w-100 px-4 fixed-top flex align-items-center justify-content-between navbar navbar-expand-lg navbar-dark bg-primary">
          <div className="d-flex flex-row w-100 justify-content-between">
            <a className="navbar-brand" href="#">
              <img className="img-fluid w-50" src={brandImage} alt="" />
            </a>

            <form>
              <div className="bg-light d-flex justify-content-center align-items-center">
                <input
                  className="search-bar form-control"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <a className="search-icon ">
                  <i className="fas fa-search"></i>
                </a>
              </div>
            </form>

            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div id="navbarSupportedContent">
              <ul className="column navbar-nav me-auto mb-2 mb-lg-0 ">
                <li className="nav-item">
                  <Login />
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link active dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    More
                  </a>
                  <ul
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdown"
                  >
                    <li>
                      <a className="dropdown-item" href="#">
                        Notification Preference
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Sell On Flipkart
                      </a>
                    </li>
                    <li>
                      <hr className="dropdown-divider" />
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        24x7 Customer Care
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="nav-item">
                  <a className="nav-link active">
                    <Link className="link" to="/cart">
                      <div className="cartItems">
                        <i class="mx-2 fas fa-shopping-cart fa-2x"></i>
                        <span className="rounded-circle cart-item">
                          {cardData.length > 0 ? cardData.length : undefined}
                        </span>
                      </div>
                    </Link>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
