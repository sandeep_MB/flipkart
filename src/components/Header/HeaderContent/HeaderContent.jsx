import React, { Component } from "react";
import "./HeaderContent.css";
import img1 from "./contentImg/img1.webp";
import img2 from "./contentImg/img2.webp";
import img3 from "./contentImg/img3.webp";
import img4 from "./contentImg/img4.webp";
import img5 from "./contentImg/img5.webp";
import img6 from "./contentImg/img6.webp";
import img7 from "./contentImg/img7.webp";
import img8 from "./contentImg/img8.webp";

export default class HeaderContent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div class="header-content container-fluid">
          <div class="row align-items-start">
            <div class="col category">
              <img className="img" src={img1} alt="" /> <p>top Offers</p>
            </div>
            <div class="col category">
              <img className="img" src={img2} alt="" /> <p>Grocery</p>
            </div>
            <div class="col category">
              <img className="img" src={img3} alt="" /> <p>Mobiles</p>
            </div>
            <div class="col category">
              <img className="img" src={img4} alt="" />{" "}
              <div class="dropdown">
                <p
                  class="dropdown-toggle"
                  id="dropdownMenuButton1"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Fashion
                </p>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                  <li>
                    <a class="dropdown-item" href="#">
                      Men's Top Wear
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Men's Bottom Wear
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Women Ethinic
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Women Western
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Men Footwear
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Watches and Accessories
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Bags, Suitcase
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div
              class="col category"
              // onMouseEnter={() => this.showDetails("electronics")}
              // onMouseLeave={() => this.hideDetails("electronics")}
            >
              <img className="img" src={img5} alt="" />
              <div class="dropdown">
                <p
                  class="dropdown-toggle"
                  id="dropdownMenuButton1"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Electronics
                </p>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                  <li>
                    <a class="dropdown-item" href="#">
                      Audio
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Camera & Accessories
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Computer & Laptops
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Gaming
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Mobile Accessories
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Powerbank
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Smart Home Automation
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col category">
              <img className="img" src={img6} alt="" />
              <div class="dropdown">
                <p
                  class="dropdown-toggle"
                  id="dropdownMenuButton1"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Home
                </p>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                  <li>
                    <a class="dropdown-item" href="#">
                      Home Furnitures
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Living Room
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Kitchen and Dining
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Bedroom
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Lighings & Electricals
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Space Saving
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Tools & Utility
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col category">
              <img className="img" src={img7} alt="" />
              <div class="dropdown">
                <p
                  class="dropdown-toggle"
                  id="dropdownMenuButton1"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Appliances
                </p>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                  <li>
                    <a class="dropdown-item" href="#">
                      Televisions
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Washing Machines
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Air Conditions
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Refrigerators
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Kitchen Appliances
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Seasonal Appliances
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Premium Appliances
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col category">
              <img className="img" src={img8} alt="" /> <p>Beauty & Toys</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
