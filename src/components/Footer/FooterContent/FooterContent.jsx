import React, { Component } from "react";
import "./FooterContent.css";

export default class FooterContent extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="stories">
          <div className="One-Stop">
            <h4>Flipkart: The One-stop Shopping Destination</h4>
            <p>
              E-commerce is revolutionizing the way we all shop in India. Why do
              you want to hop from one store to another in search of the latest
              phone when you can find it on the Internet in a single click? Not
              only mobiles. Flipkart houses everything you can possibly imagine,
              from trending electronics like laptops, tablets, smartphones, and
              mobile accessories to in-vogue fashion staples like shoes,
              clothing and lifestyle accessories; from modern furniture like
              sofa sets, dining tables, and wardrobes to appliances that make
              your life easy like washing machines, TVs, ACs, mixer grinder
              juicers and other time-saving kitchen and small appliances; from
              home furnishings like cushion covers, mattresses and bedsheets to
              toys and musical instruments, we got them all covered. You name
              it, and you can stay assured about finding them all here. For
              those of you with erratic working hours, Flipkart is your best
              bet. Shop in your PJs, at night or in the wee hours of the
              morning. This e-commerce never shuts down. What's more, with our
              year-round shopping festivals and events, our prices are
              irresistible. We're sure you'll find yourself picking up more than
              what you had in mind. If you are wondering why you should shop
              from Flipkart when there are multiple options available to you,
              well, the below will answer your question.
            </p>
          </div>

          <div>
            <h4>Flipkart Plus</h4>
            <p>
              A world of limitless possibilities awaits you - Flipkart Plus was
              kickstarted as a loyalty reward programme for all its regular
              customers at zero subscription fee. All you need is 500 supercoins
              to be a part of this service. For every 100 rupees spent on
              Flipkart order, Plus members earns 4 supercoins & non-plus members
              earn 2 supercoins. Free delivery, early access during sales and
              shopping festivals, exchange offers and priority customer service
              are the top benefits to a Flipkart Plus member. In short, earn
              more when you shop more! What's more, you can even use the
              Flipkart supercoins for a number of exciting services, like: An
              annual Zomato Gold membership An annual Hotstar Premium membership
              6 months Gaana plus subscription Rupees 550 instant discount on
              flights on ixigo Check out
              https://www.flipkart.com/plus/all-offers for the entire list.
              Terms and conditions apply.
            </p>
          </div>
          <div>
            <h4>No Cost EMI</h4>
            <p>
              In an attempt to make high-end products accessible to all, our No
              Cost EMI plan enables you to shop with us under EMI, without
              shelling out any processing fee. Applicable on select mobiles,
              laptops, large and small appliances, furniture, electronics and
              watches, you can now shop without burning a hole in your pocket.
              If you've been eyeing a product for a long time, chances are it
              may be up for a no cost EMI. Take a look ASAP! Terms and
              conditions apply.
            </p>
          </div>
          <div>
            <h4>EMI on Debit Cards</h4>
            <p>
              Did you know debit card holders account for 79.38 crore in the
              country, while there are only 3.14 crore credit card holders?
              After enabling EMI on Credit Cards, in another attempt to make
              online shopping accessible to everyone, Flipkart introduces EMI on
              Debit Cards, empowering you to shop confidently with us without
              having to worry about pauses in monthly cash flow. At present, we
              have partnered with Axis Bank, HDFC Bank, State Bank of India and
              ICICI Bank for this facility. More power to all our shoppers!
              Terms and conditions apply.
            </p>
          </div>
          <div>
            <h4>Mobile Exchange Offers</h4>
            <p>
              Get an instant discount on the phone that you have been eyeing on.
              Exchange your old mobile for a new one after the Flipkart experts
              calculate the value of your old phone, provided it is in a working
              condition without damage to the screen. If a phone is applicable
              for an exchange offer, you will see the 'Buy with Exchange' option
              on the product description of the phone. So, be smart, always opt
              for an exchange wherever possible. Terms and conditions apply.
            </p>
          </div>
          <div>
            <h4>Mobile Phones</h4>
            <p>
              From budget phones to state-of-the-art smartphones, we have a
              mobile for everybody out there. Whether you're looking for larger,
              fuller screens, power-packed batteries, blazing-fast processors,
              beautification apps, high-tech selfie cameras or just large
              internal space, we take care of all the essentials. Shop from top
              brands in the country like Samsung, Apple, Oppo, Xiaomi, Realme,
              Vivo, and Honor to name a few. Rest assured, you're buying from
              only the most reliable names in the market. What's more, with
              Flipkart's Complete Mobile Protection Plan, you will never again
              find the need to run around service centres. This plan entails you
              to a number of post-purchase solutions, starting at as low as
              Rupees 99 only! Broken screens, liquid damage to phone, hardware
              and software glitches, and replacements - the Flipkart Complete
              Mobile Protection covers a comprehensive range of post-purchase
              problems, with door-to-door services.
            </p>
          </div>
        </div>

        <div>
          <h5>Top Stories:Brand Directory</h5>
          <p>
            MOST SEARCHED FOR ON FLIPKART:iPhone 13iPhone | 13 ProiPhone 13 |
            Pro MaxiPhone 13 | MiniFlipkart | QuickBooks | Flipkart | Help
            CentreFlipkart | Track OrdersFlipkart | Manage Orders | Flipkart |
            Return Orders | Flipkart Gift Cards Store | Flipkart Axis Bank
            Credit Card | Flipkart Pay Later
          </p>
          <p>
            MOBILES:iPhone 12 64GB | iPhone 12 Pro 512GB | iPhone 12 128GB |
            Vivo Y91i |Vivo Y11 | Vivo Y15 | Vivo Y50 | Vivo Y12 | Reno2 FOppo
            A12 | Oppo F15 | Oppo A31 | Samsung A71 | Samsung A51 | Samsung A31
            | Realme X2 | iPhone 11 | iPhone 11 Pro | iPhone 11 Pro Max | Mobile
            Offers | iphone x4G Mobile | Nokia Mobile | Samsung Mobile | iphone
            | Oppo Mobile | Vivo Mobile
          </p>
          <p>
            CAMERA:GoPro Action Camera | Nikon Camera | Canon Camera | Sony
            Camera | Canon DSLR | Nikon DSLR
          </p>
          <p>
            LAPTOPS:Microsoft Surface Go Pentium Gold 64GB | Microsoft Surface
            Go Pentium 128GB | Branded Laptops | Apple Laptops | Acer Laptops |
            Lenovo Laptops | Dell Laptops | Asus Laptops | HP Laptops | Gaming
            Laptops2 in 1 Laptops | Business Laptops
          </p>
          <p>
            TVS:Nokia TV | Panasonic TV | Thomson TV | Vu TV | Realme TV |
            Motorola TV | OnePlus TVs | LG TV | TVSony TV |Samsung TV | Android
            Television | Iffalcon Tv | Mi TV
          </p>
          <p>
            LARGE APPLIANCES:Washing Machines | Refrigerators | Air Conditioners
            | Electric Cookers |Electric Jug(Heater) / Travel Kettles |
            Induction Cooktops | Inverters / stabilizerIrons / Iron Box | Mixer
            Grinder JuicerWet Grinders | Chimneys | Microwave OvensVacuum
            CleanersWater PurifierFan
          </p>
          <p>
            CLOTHING:Men Shirts | Kurta Pajama | KurtasMen T-ShirtsJeans |
            SareeDresses | Kids Dresses | Designer Salwar Suits |Bra | Designer
            Kurtis | Track PantMen Kurtas | Gym Wear | Party Dresses | Palazzo
            Suits | Boys Clothing | GlovesNighty | Maxi Dresses | Anarkali |
            Gowns | Culottes | Salwar Suits | Kurtis | Designer Sarees |
            Leggings | Shorts | Georgette Sarees | Ethnic WearBriefs & Trunks
          </p>
          <p>
            GROCERIES:PhonePe Grocery Voucher | Hand WashSoap | Cashew Nuts |
            Sunflower Oil | Eggs | Toilet Cleaner | Harpic Toilet Cleaner |
            Dettol Soap | Mustard Oil | Biscuits | Cheese | Patanjali Atta |
            Fortune Oil | Aashirvaad Atta | Tea
          </p>
          <p>
            CAMERA:GoPro Action Camera | Nikon Camera | Canon Camera | Sony
            Camera | Canon DSLR | Nikon DSLR
          </p>
        </div>
      </div>
    );
  }
}
