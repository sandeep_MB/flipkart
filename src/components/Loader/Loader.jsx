import React, { Component } from "react";
import img1 from "./img/ShoppingCart.gif";
import "../../App.css";
class Loader extends Component {
  render() {
    return (
      <div className="loader center">
        <img src={img1} alt="" />
      </div>
    );
  }
}

export default Loader;
