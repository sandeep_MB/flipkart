import React, { Component } from "react";
// import "./nav.css";
// import "./log.css";
import { Link } from "react-router-dom";
import ReactModal from "react-modal";
import validator from "validator";

export default class Nav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleModal: false,
      toggleModalSignup: false,
      userName: "",
      userAge: "",
      userEmail: "",
      userPassword: "",
      confirmPassword: "",
      checkbtn: false,
      currentStatus: " ",
      setClassName: "d-none",
      errors: {},
      loginError: false,
      userLoggedIn: "",
      loggedIn: false,
    };

    this.customStyles = {
      content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
      },
    };

    this.emailLogin = React.createRef();
    this.passLogin = React.createRef();
  }

  handleChanges = (event) => {
    let value = event.target.value;

    if (event.target.type === "checkbox") {
      value = event.target.checked;
    }

    this.setState({
      [event.target.id]: value,
    });
  };

  formValidation = () => {
    const {
      userName,
      userAge,
      userEmail,
      userPassword,
      confirmPassword,
      checkbtn,
    } = this.state;
    const error = {};

    let isValid = true;

    if (validator.isEmpty(userName)) {
      error.name = "Please enter your name.";
      isValid = false;
    } else if (
      !validator.isLength(userName, { min: 3, max: undefined }) ||
      !validator.isAlpha(userName, "en-US", { ignore: " -" })
    ) {
      error.name = "Name must be 3 characters or more.";
      isValid = false;
    }

    if (validator.isEmpty(userAge)) {
      error.age = "Please enter your age.";
    } else if (validator.isInt(userAge, { min: 19, max: 99 }) === false) {
      error.age = "Age must be greater than 18 and less than 100.";
      isValid = false;
    }

    if (!validator.isEmail(userEmail)) {
      error.userEmail = "Please enter valid Email-id.";
      isValid = false;
    }

    if (validator.isEmpty(userPassword)) {
      error.password = "Please enter your password.";
      isValid = false;
    } else if (
      validator.isStrongPassword(userPassword, {
        minLength: 8,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1,
      }) === false
    ) {
      error.password =
        "Password:Min 8 char,1 lowecase,1 uppercase,1 symbol & 1 No.";
      isValid = false;
    }

    if (validator.isEmpty(confirmPassword)) {
      error.confirmPassword = "Please enter your password again.";
      isValid = false;
    }

    if (validator.equals(confirmPassword, userPassword) === false) {
      error.confirmPassword = "Password is not same. Please try again.";
      isValid = false;
    }

    if (checkbtn === false) {
      error.checkboxValue = "Click the checkbox button to confirm agreement.";
      isValid = false;
    }

    this.setState({
      errors: error,
    });

    return isValid;
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const valid = this.formValidation();

    if (valid) {
      let tempArr = window.localStorage["zomato"];
      if (!tempArr) {
        tempArr = [];
      } else {
        tempArr = JSON.parse(tempArr);
      }
      tempArr.push({
        name: this.state.userName,
        email: this.state.userEmail,
        pass: this.state.userPassword,
      });
      window.localStorage["zomato"] = JSON.stringify(tempArr);
      this.setState({
        currentStatus: "Congratulations, Form submitted!",
        setClassName: "alert alert-success p-3",
        userName: "",
        userAge: "",
        userEmail: "",
        userPassword: "",
        confirmPassword: "",
        checkbtn: false,
        toggleModalSignup: false,
        toggleModal: true,
      });
    } else {
      this.setState({
        currentStatus: "",
        setClassName: "d-none",
      });
    }
  };

  toggleModalFunc = () => {
    const toggle = this.state.toggleModal;
    this.setState({
      toggleModal: !toggle,
      toggleModalSignup: false,
    });
  };

  toggleModalSignupFunc = () => {
    const toggle = this.state.toggleModalSignup;
    this.setState({
      toggleModalSignup: !toggle,
      toggleModal: false,
    });
  };

  validateLogin = () => {
    const emailId = this.emailLogin.current.value;
    const passw = this.passLogin.current.value;
    let account = window.localStorage["zomato"];
    if (!emailId || !passw) {
      this.setState({
        loginError: true,
      });
      return;
    }
    if (!account) {
      this.setState({
        loginError: true,
      });
      return;
    } else {
      account = JSON.parse(account);
      const result = account.find((ele) => {
        return ele.email === emailId && ele.pass === passw;
      });
      if (result === undefined) {
        this.setState({
          loginError: true,
        });
        return;
      }
      this.setState({
        userLoggedIn: result,
        toggleModal: false,
        loggedIn: true,
      });
    }
  };

  render() {
    return (
      <div className="display-row nav-height">
        <div>
          <ul className="nav-ul">
            <button
              onClick={this.toggleModalFunc}
              className="btn btn-outline-dark"
              title={
                this.state.loggedIn ? this.state.userLoggedIn.email : undefined
              }
            >
              {this.state.loggedIn ? this.state.userLoggedIn.name : "Login"}
            </button>
            <ReactModal
              isOpen={this.state.toggleModal}
              style={this.customStyles}
            >
              <span
                className="d-flex justify-content-end m-0"
                onClick={this.toggleModalFunc}
                style={{ cursor: "pointer" }}
              >
                X
              </span>
              <h4 className="text-center">Login</h4>
              {this.state.loginError && (
                <div className="text-danger">Account not found</div>
              )}
              <div className="d-flex flex-column" style={{ gap: "20px" }}>
                <input
                  type="email"
                  placeholder="Enter Email Address"
                  className="form-control"
                  ref={this.emailLogin}
                />
                <input
                  type="password"
                  placeholder="Enter Your Password"
                  className="form-control"
                  ref={this.passLogin}
                />
                <button
                  className="btn btn-primary"
                  onClick={this.validateLogin}
                >
                  Login
                </button>
                <span>
                  Don't have an account?{" "}
                  <span
                    onClick={this.toggleModalSignupFunc}
                    style={{
                      color: "blue",
                      textDecoration: "underline",
                      cursor: "pointer",
                    }}
                    onClick={this.toggleModalSignupFunc}
                  >
                    Sign Up
                  </span>
                </span>
              </div>
            </ReactModal>

            <ReactModal
              isOpen={this.state.toggleModalSignup}
              style={this.customStyles}
            >
              <div>
                <span
                  className="d-flex justify-content-end m-0"
                  style={{ cursor: "pointer" }}
                  onClick={this.toggleModalSignupFunc}
                >
                  X
                </span>
                <div>
                  <section className={this.state.setClassName}>
                    {this.state.currentStatus}
                  </section>

                  <section className="Form my-5">
                    {/* <section className='Form my-5 bg-dark'> */}

                    <div className="container my-5 pt-1 d-flex justify-content-center .justify-content-lg-start mt-2 pt-2 border border-dark-2">
                      <div className="col-lg-9 col-md-7 px-2">
                        <form>
                          <div className="d-block d-sm-block d-md-block d-lg-none col-sm-3 col-3 offset-5 d-flex justify-content-center align-items-center align-self-center border">
                            {/* <img className="img-responsive img-fluid img-thumbnail border-0 " src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLAvTdqXwWiQOnU2YFPDPfUshrnyvmRiR4Ow&usqp=CAU' />  */}
                          </div>

                          <h1 className="text-center">Sign up</h1>

                          <div className="row ">
                            <input
                              className="my-2 p-2 border form-control"
                              type="text"
                              id="userName"
                              placeholder="Enter Name"
                              value={this.state.userName}
                              onChange={this.handleChanges}
                            />
                          </div>
                          <div
                            className={
                              this.state.errors.name
                                ? "alert-danger my-0.5 border-danger .text-wrap "
                                : "no-status"
                            }
                          >
                            {this.state.errors.name}
                          </div>

                          <div className="row">
                            <input
                              className="my-2 p-2 border form-control"
                              type="number"
                              id="userAge"
                              placeholder="Enter Age"
                              value={this.state.userAge}
                              onChange={this.handleChanges}
                            />
                          </div>
                          <div
                            className={
                              this.state.errors.age
                                ? "alert-danger my-0.5 border-danger .text-wrap "
                                : "no-status"
                            }
                          >
                            {this.state.errors.age}
                          </div>

                          <div className="row">
                            <input
                              className="my-2 p-2 border form-control"
                              type="email"
                              id="userEmail"
                              placeholder="Enter Email-id"
                              value={this.state.userEmail}
                              onChange={this.handleChanges}
                            />
                          </div>
                          <div
                            className={
                              this.state.errors.userEmail
                                ? "alert-danger my-0.5border-danger .text-wrap "
                                : "no-status"
                            }
                          >
                            {this.state.errors.userEmail}
                          </div>

                          <div className="row">
                            <input
                              className="my-2 p-2 border form-control"
                              type="password"
                              id="userPassword"
                              placeholder="Enter Password"
                              value={this.state.userPassword}
                              onChange={this.handleChanges}
                            />
                          </div>
                          <div
                            className={
                              this.state.errors.password
                                ? "alert-danger my-0.5 border-danger .text-wrap "
                                : "no-status"
                            }
                          >
                            {this.state.errors.password}
                          </div>

                          <div className="row">
                            <input
                              className="my-2 p-2 border form-control"
                              type="password"
                              id="confirmPassword"
                              placeholder="Confirm Password"
                              value={this.state.confirmPassword}
                              onChange={this.handleChanges}
                            />
                          </div>
                          <div
                            className={
                              this.state.errors.confirmPassword
                                ? "alert-danger my-0.5 border-danger .text-wrap "
                                : "no-status"
                            }
                          >
                            {this.state.errors.confirmPassword}
                          </div>

                          <div className="row d-flex flex-column">
                            <input
                              className="my-2 p-2 border"
                              type="checkbox"
                              id="checkbtn"
                              onClick={this.handleChanges}
                            />

                            <span className="text-center mx-2">
                              {" "}
                              Accept terms and conditions
                            </span>

                            <div
                              className={
                                this.state.errors.checkboxValue
                                  ? "alert-danger my-0.5 border-danger .text-wrap "
                                  : "no-status"
                              }
                            >
                              {this.state.errors.checkboxValue}
                            </div>
                          </div>

                          <div className="row">
                            <button
                              onClick={this.handleSubmit}
                              className="btn-primary .text-wrap  my-3 p-1.5"
                            >
                              Sign up
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </ReactModal>

            <div
              className="modal  fade "
              id="exampleModal"
              tabIndex="1"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog modal-dialog-centered">
                + d="M4 d="M4 d="M4
                <div className="modal-content ">
                  <div className="modal-header border-0">
                    <div className="font-24">Log in</div>
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <div className="modal-body  d-flex flex-column">
                    <div className="text-center w-100 border rounded py-3 d-flex my-3 ">
                      <div className="d-flex mx-2 align-items-center">
                        <div className="indian-flag">
                          <img src="https://b.zmtcdn.com/images/flags_z10/in.png" />
                        </div>
                        <div className="mx-2 d-flex border-end align-items-center">
                          +91{" "}
                          <div>
                            <i className="far fa-chevron-down"></i>
                          </div>
                        </div>
                      </div>
                      <div className="w-100 mx-0 ">
                        <input
                          placeholder="Phone number"
                          type="number"
                          className="border-0  w-100"
                          maxLength={10}
                        />
                      </div>
                    </div>
                    <div className="text-center W-100 otp-button">
                      <button className="w-100 bg-secondary text-white border-0 rounded py-3">
                        Send OTP
                      </button>
                    </div>
                    <div className="text-center w-100 d-flex">
                      <div className="w-50">
                        <hr className="w-100" />
                      </div>
                      <div className="px-2">or</div>
                      <div className="w-50">
                        <hr />
                      </div>
                    </div>
                    <div className="text-center border rounded w-100 py-3 my-3">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="#EF4F5F"
                        width="24"
                        height="24"
                        viewBox="0 0 20 20"
                        aria-labelledby="icon-svg-title- icon-svg-desc-"
                        role="img"
                        class="sc-rbbb40-0 fmIpur"
                      >
                        <title>mail-fill</title>
                        <path d="M10 9.58c-1.62 0-10-4.76-10-4.76v-0.74c0-0.92 0.74-1.66 1.66-1.66h16.68c0.92 0 1.66 0.74 1.66 1.66l-0.020 0.84c0 0-8.28 4.66-9.98 4.66zM10 11.86c1.78 0 9.98-4.46 9.98-4.46l0.020 10c0 0.92-0.74 1.66-1.66 1.66h-16.68c-0.92 0-1.66-0.74-1.66-1.66l0.020-10c0 0 8.36 4.46 9.98 4.46z"></path>
                      </svg>
                      <span className="px-3">Continue with Email</span>
                    </div>
                    <div className="text-center border rounded w-100 py-3">
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 22 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M4.87566 13.2946L4.10987 16.1534L1.31093 16.2126C0.474461 14.6611 0 12.886 0 10.9997C0 9.17565 0.443609 7.45552 1.22994 5.94092H1.23054L3.72238 6.39776L4.81396 8.87465C4.5855 9.54071 4.46097 10.2557 4.46097 10.9997C4.46106 11.8072 4.60732 12.5808 4.87566 13.2946Z"
                          fill="#FBBB00"
                        ></path>
                        <path
                          d="M21.8082 8.94507C21.9345 9.61048 22.0004 10.2977 22.0004 11C22.0004 11.7875 21.9176 12.5557 21.7598 13.2967C21.2243 15.8183 19.8252 18.0201 17.8869 19.5782L17.8863 19.5776L14.7477 19.4175L14.3035 16.6445C15.5896 15.8902 16.5947 14.7098 17.1242 13.2967H11.2422V8.94507H17.21H21.8082Z"
                          fill="#518EF8"
                        ></path>
                        <path
                          d="M17.8865 19.5778L17.8871 19.5784C16.002 21.0937 13.6073 22.0002 11.0006 22.0002C6.81152 22.0002 3.16945 19.6588 1.31152 16.2132L4.87625 13.2952C5.8052 15.7744 8.19679 17.5392 11.0006 17.5392C12.2057 17.5392 13.3348 17.2134 14.3036 16.6447L17.8865 19.5778Z"
                          fill="#28B446"
                        ></path>
                        <path
                          d="M18.0208 2.53241L14.4573 5.44981C13.4546 4.82307 12.2694 4.46102 10.9996 4.46102C8.13229 4.46102 5.69596 6.30682 4.81356 8.87494L1.23009 5.9412H1.22949C3.06022 2.41154 6.74823 0 10.9996 0C13.6686 0 16.1158 0.950726 18.0208 2.53241Z"
                          fill="#F14336"
                        ></path>
                      </svg>
                      <span className="px-3">Continue with Google</span>
                    </div>
                  </div>
                  <div className="modal-footer border justify-content-start">
                    New to Zomato?{" "}
                    <span className="red-color">Create account</span>
                  </div>
                </div>
              </div>
            </div>
          </ul>
        </div>
      </div>
    );
  }
}
