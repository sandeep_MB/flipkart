import React, { Component } from "react";
import "./CartItems.css";

export default class CartItem extends Component {
  render() {
    const cardData = this.props.data.productReducer.cardData;
    return (
      <div className="mt-4">
        <h2> Cart Items</h2>
        <div className="py-4 container cart-container">
          {cardData.map((data) => {
            return (
              <div>
                <div className="cart-product col d-flex justify-content-around my-4">
                  <div className="mx-4 my-4">
                    <img className="image" src={data.image} alt="" />
                    <h5>{data.title.substring(0, 15)}</h5>
                  </div>
                  <h3 className="price">Price ${data.price}</h3>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
