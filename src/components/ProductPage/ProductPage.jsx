import React, { Component } from "react";
import Loader from "../Loader/Loader";
import "./ProductPage.css";
import { Link } from "react-router-dom";

class ProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryData: [],
      filterData: [],
      filterDataError: false,
      all: false,
      flag: false,
      cartError: false,
    };
    this.inputdata = React.createRef();
  }

  filterData(name) {
    if (name === "all") {
      this.setState({ flag: false });
    } else {
      const category = this.props.data.productReducer.products.filter(
        (product) => product.category === name
      );
      this.setState({ categoryData: category, flag: true });
    }
  }

  addItemToCart(data) {
    const cardArray = this.props.data.productReducer.cardData;
    const flag = cardArray.filter((item) => item.id === data.id);
    if (flag.length === 0) {
      cardArray.push(data);
      this.props.addDataToCart(cardArray);
    }
  }

  handleChange(e) {
    console.log();
    if (!(e.target.value == "")) {
      const category = this.props.data.productReducer.products.filter((value) =>
        value.title.toLowerCase().includes(e.target.value.toLowerCase())
      );
      // console.log(category);
      this.setState({ filterData: category, filterDataError: true });
    }
  }

  getApi(url) {
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        this.props.getProductData({
          products: data,
          error: false,
          loader: false,
        });
      })
      .catch((err) => console.error(err));
  }

  componentDidMount() {
    this.getApi("https://fakestoreapi.com/products/");
  }

  putData = (e) => {
    this.inputdata.current.value = e.target.innerText;
  };

  render() {
    let { products, error, loader } = this.props.data.productReducer;
    console.log(this.props.data);
    if (this.state.flag) {
      products = this.state.categoryData;
    }

    if (loader) {
      return <Loader />;
    }
    return (
      <section className="p-5">
        {!error ? (
          <div className="album py-5 bg-light px-3">
            <h3 className="mb-4">
              <span className="mx-2 badge bg-primary">New</span>Arrivals
            </h3>
            <div className="d-flex justify-content-between align-items-center flex-column">
              <input
                className="  input-field form-control"
                type="search"
                onChange={(e) => this.handleChange(e)}
                defaultValue=""
                ref={this.inputdata}
              />
              <div className="text">
                {this.state.filterDataError
                  ? this.state.filterData.map((f) => {
                      return (
                        <>
                          <p
                            onClick={this.putData}
                            style={{ cursor: "pointer" }}
                          >
                            {f.title}
                          </p>
                        </>
                      );
                    })
                  : undefined}
              </div>
            </div>
            <div className="d-flex justify-content-center">
              <a
                href="#"
                className="btn filterbtn mx-4 my-2"
                role="button"
                data-bs-toggle="button"
                onClick={() => this.filterData("all")}
              >
                All
              </a>
              <a
                href="#"
                className="btn filterbtn mx-4 my-2"
                role="button"
                data-bs-toggle="button"
                onClick={() => this.filterData("men's clothing")}
              >
                Men's Wear
              </a>
              <a
                href="#"
                className="btn filterbtn mx-4 my-2"
                role="button"
                data-bs-toggle="button"
                onClick={() => this.filterData("women's clothing")}
              >
                Women's Wear
              </a>
              <a
                href="#"
                className="btn filterbtn mx-4 my-2"
                role="button"
                data-bs-toggle="button"
                onClick={() => this.filterData("jewelery")}
              >
                Jewellery
              </a>
              <a
                href="#"
                className="btn filterbtn mx-4 my-2"
                role="button"
                data-bs-toggle="button"
                onClick={() => this.filterData("electronics")}
              >
                Electronics
              </a>
            </div>

            <div className="row my-4 row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              {/* Column */}
              {products.map((product) => {
                return (
                  <div key={product.id} className="col-lg-3">
                    <div className="h-100 card shadow-sm d-flex justify-content-center align-items-center">
                      <img
                        className="product-image w-50 h-50 bd-placeholder-img card-img-top py-4"
                        height="225"
                        src={product.image}
                        aria-label="Placeholder: Thumbnail"
                      />
                      <p
                        className="mt-4"
                        x="50%"
                        y="50%"
                        fill="#eceeef"
                        dy=".3em"
                      >
                        <i className="bi bi-tags-fill"></i>$ {product.price}
                      </p>

                      <div className="card-body d-flex justify-content-between flex-column">
                        <p className="card-text">
                          <strong>
                            <Link
                              className="link"
                              to={`/products/${product.id}`}
                            >
                              {product.title.substring(0, 25)}
                            </Link>
                          </strong>
                        </p>
                        <div className="d-flex justify-content-center align-items-center">
                          <div className="btn-group">
                            <button
                              type="button"
                              className="btn btn-sm cart-btn"
                              onClick={() => {
                                this.addItemToCart(product);
                              }}
                            >
                              Add To Cart
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        ) : undefined}
      </section>
    );
  }
}

export default ProductPage;
