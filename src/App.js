import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./components/Home/Home";
import Login from "./components/Login/Login";
import ViewProduct from "./Container/ProductContainer";
import CartItems from "./Container/CartItemContainer";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/signin" element={<Login />} />
        <Route path="/products/:id" element={<ViewProduct />} />
        <Route path="/cart" element={<CartItems />} />
      </Routes>

      {/* <HeaderContent /> */}
      {/* <Carousel /> */}
      {/* <DOD /> */}
      {/* <ProductPage /> */}
      {/* <FooterContent /> */}
      {/* <Footer /> */}
    </div>
  );
}

export default App;
